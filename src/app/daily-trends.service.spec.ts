import { TestBed } from '@angular/core/testing';

import { DailyTrendsService } from './daily-trends.service';

describe('DailyTrendsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyTrendsService = TestBed.get(DailyTrendsService);
    expect(service).toBeTruthy();
  });
});
