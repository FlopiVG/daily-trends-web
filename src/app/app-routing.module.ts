import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DailyTrendsComponent } from './daily-trends/daily-trends.component';
import { DailyTrendsDetailComponent } from './daily-trends-detail/daily-trends-detail.component';

const routes: Routes = [
  { path: '', component: DailyTrendsComponent },
  { path: ':id', component: DailyTrendsDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
