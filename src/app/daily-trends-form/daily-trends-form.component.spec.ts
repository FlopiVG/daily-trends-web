import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyTrendsFormComponent } from './daily-trends-form.component';

describe('DailyTrendsFormComponent', () => {
  let component: DailyTrendsFormComponent;
  let fixture: ComponentFixture<DailyTrendsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyTrendsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyTrendsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
