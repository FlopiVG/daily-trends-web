import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DailyTrendsService } from '../daily-trends.service';

@Component({
  selector: 'app-daily-trends-form',
  templateUrl: './daily-trends-form.component.html',
  styleUrls: ['./daily-trends-form.component.css']
})
export class DailyTrendsFormComponent implements OnInit {
  createOpen = false;
  form = new FormGroup({
    title: new FormControl(''),
    body: new FormControl(''),
    image: new FormControl('')
  });

  constructor(private dailyTrendsService: DailyTrendsService) {}

  ngOnInit() {}

  toggleCreate() {
    this.createOpen = !this.createOpen;
  }

  onSubmit() {
    this.dailyTrendsService.createFeed(this.form.value).subscribe(feed => {
      this.form.reset();
      this.toggleCreate();
    });
  }
}
