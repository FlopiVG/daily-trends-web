import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DailyTrendsService } from '../daily-trends.service';
import { Feed } from '../feed';

@Component({
  selector: 'app-daily-trends',
  templateUrl: './daily-trends.component.html',
  styleUrls: ['./daily-trends.component.css']
})
export class DailyTrendsComponent implements OnInit {
  feeds: Feed[];
  loading = true;

  constructor(
    private readonly dailyTrendsService: DailyTrendsService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.dailyTrendsService.getTodayFeeds().subscribe(feeds => {
      this.feeds = feeds;
      this.loading = false;
    });
  }

  detailsFeed(id) {
    this.router.navigate([id]);
  }

  removeFeed(id) {
    this.dailyTrendsService.removeFeed(id).subscribe(() => {
      this.feeds = this.feeds.filter(feed => feed._id !== id);
    });
  }
}
