import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DailyTrendsComponent } from './daily-trends.component';
import { DailyTrendsService } from '../daily-trends.service';
import { DailyTrendsFormComponent } from '../daily-trends-form/daily-trends-form.component';
import { RouterModule } from '@angular/router';
import { MatSpinner } from '@angular/material/progress-spinner';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DailyTrendsComponent, MatSpinner, DailyTrendsFormComponent],
  providers: [DailyTrendsService],
  imports: [CommonModule, HttpClientModule, RouterModule, ReactiveFormsModule],
  exports: [DailyTrendsComponent]
})
export class DailyTrendsModule {}
