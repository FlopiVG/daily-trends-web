import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyTrendsComponent } from './daily-trends.component';

describe('DailyTrendsComponent', () => {
  let component: DailyTrendsComponent;
  let fixture: ComponentFixture<DailyTrendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyTrendsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyTrendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
