export interface Feed {
  readonly _id: string;
  readonly title: string;
  readonly body: string;
  readonly image: string;
  readonly source: string;
  readonly publisher: string;
}
