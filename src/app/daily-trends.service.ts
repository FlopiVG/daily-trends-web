import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Feed } from './feed';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DailyTrendsService {
  constructor(private readonly http: HttpClient) {}

  getTodayFeeds(): Observable<Feed[]> {
    return this.http.get<Feed[]>(
      'http://localhost:3000/api/v1/dailytrends/today'
    );
  }

  getFeed(id): Observable<Feed> {
    return this.http.get<Feed>(
      `http://localhost:3000/api/v1/dailytrends/get/${id}`
    );
  }

  removeFeed(id): Observable<Feed> {
    return this.http.delete<Feed>(
      `http://localhost:3000/api/v1/dailytrends/${id}`
    );
  }

  createFeed(feedDto): Observable<Feed> {
    return this.http.post<Feed>(
      'http://localhost:3000/api/v1/dailytrends',
      feedDto
    );
  }
}
