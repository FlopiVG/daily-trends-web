import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyTrendsDetailComponent } from './daily-trends-detail.component';

describe('DailyTrendsDetailComponent', () => {
  let component: DailyTrendsDetailComponent;
  let fixture: ComponentFixture<DailyTrendsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyTrendsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyTrendsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
