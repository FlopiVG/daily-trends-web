import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DailyTrendsService } from '../daily-trends.service';
import { Feed } from '../feed';

@Component({
  selector: 'app-daily-trends-detail',
  templateUrl: './daily-trends-detail.component.html',
  styleUrls: ['./daily-trends-detail.component.css'],
  providers: [DailyTrendsService]
})
export class DailyTrendsDetailComponent implements OnInit {
  private feed: Feed = null;

  constructor(
    private dailyTrendsService: DailyTrendsService,
    private route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.dailyTrendsService.getFeed(params['id']).subscribe(feed => {
        this.feed = feed;
      });
    });
  }

  goBack() {
    this.router.navigate(['/']);
  }
}
